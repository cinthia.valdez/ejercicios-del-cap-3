# Autora Cinthia Valdez
# Resolucion de ejercicio 2
#  Reescribe el programa del salario usando try y except,
#  de modo que el programa sea capaz de gestionar entradas
#  no numéricas con elegancia, mostrando un mensaje y
#  saliendo del programa.

try:
    horas = int(input("ingrese el numero de horas que trabaja"))
    tarifa = float(input("ingrese su tarifa por hora"))
    salario = float(horas * tarifa)

    if horas > 40:
        horasex = horas - 40
        salario = (horas - horasex) * tarifa
        pagoext = (tarifa * 1.5) * horasex
        pagototal = pagoext + salario
        print('Su salario con horas extras es de  : ', pagototal)
    elif horas <= 40:
        print('El salario sin horas extras es de : ', salario)
except:
    print("Error introduzca un número")