# Autora Cinthia Valdez
# Calcular valor a pagar si trabaja mas de 40 horas
# Ejercicio 1: Reescribe el programa del cálculo del salario para darle al empleado 1.5 veces
# la tarifa horaria para todas las horas trabajadas que excedan de 40

horas = int(input("ingrese el numero de horas que trabaja"))
tarifa = float(input("ingrese su tarifa por hora"))
salario = float(horas * tarifa)
if horas > 40:
    horasex = horas - 40
    pagoext = (tarifa * 1.5) * horasex
    salario = (horas - horasex) * tarifa
    pagototal = pagoext + salario
    print('Su salario con horas extras es de : ', pagototal)
elif horas <= 40:
    print('El salario sin horas extras es de : ', salario)